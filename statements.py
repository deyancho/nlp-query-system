# File: statements.py
# Template file for Informatics 2A Assignment 2:
# 'A Natural Language Query System in Python/NLTK'

# John Longley, November 2012
# Revised November 2013 and November 2014 with help from Nikolay Bogoychev
# Revised November 2015 by Toms Bergmanis
# Revised October 2017 by Chunchuan Lyu

# PART A: Processing statements

def add(lst,item):
    if (item not in lst):
        lst.insert(len(lst),item)

class Lexicon:
    """stores known word stems of various part-of-speech categories"""
    # add code here
    def __init__(self):
        self.lexicon = {}

    def add(self, stem, cat):
        if cat not in ['P', 'N', 'A', 'I', 'T']:
            pass
        else:
            if cat not in self.lexicon.keys():
                self.lexicon[cat] = [stem]
            else:
                if stem not in self.lexicon[cat]:
                    self.lexicon[cat].append(stem)


    def getAll(self, cat):
        if cat not in self.lexicon.keys():
            return []
        else:
            return self.lexicon[cat]


class FactBase:
    """stores unary and binary relational facts"""
    # add code here
    def __init__(self):
        self.unary_facts = {}
        self.binary_facts = {}

    def addUnary(self, pred, e1):
        if pred not in self.unary_facts.keys():
            self.unary_facts[pred] = [e1]
        else:
            if e1 not in self.unary_facts[pred]:
                self.unary_facts[pred].append(e1)

    def addBinary(self, pred, e1, e2):
        if pred not in self.binary_facts.keys():
            self.binary_facts[pred] = [(e1, e2)]
        else:
            if (e1, e2) not in self.binary_facts[pred]:
                self.binary_facts[pred].append((e1, e2))

    def queryUnary(self, pred, e1):
        return e1 in self.unary_facts[pred]

    def queryBinary(self, pred, e1, e2):
        return (e1, e2) in self.binary_facts[pred]

import re
from nltk.corpus import brown


def return_stem_or_not(stem, third_form_in_dict):
    if third_form_in_dict:
        return stem
    else:
        if (stem, "VB") in brown.tagged_words():
            return stem
        else:
            return ""

def verb_stem(s):
    """extracts the stem from the 3sg form of a verb, or returns empty string"""
    # add code here

    # Negative lookahead between 2 matches
    # https://stackoverflow.com/questions/9843338/regex-negative-look-ahead-between-two-matches

    is_word = False
    if s.isalpha():
        is_word = True

    if not is_word:
        return ""

    third_sg_in_dict = False
    if (s, "VBZ") in brown.tagged_words():
        third_sg_in_dict = True

    if re.search(r"(?<!(s|x|y|z|a|e|i|o|u))s$", s) and re.search(r"(?<!(ch|sh))s$", s):
        print('R1')
        hypothesised_stem = s[:-1]
        return_stem = return_stem_or_not(hypothesised_stem, third_sg_in_dict)
        return return_stem

    if re.search(r"(?<=[a|e|i|o|u])ys$", s):
        print('R2')
        hypothesised_stem = s[:-1]
        return_stem = return_stem_or_not(hypothesised_stem, third_sg_in_dict)
        return return_stem

    if s.endswith("ies"):
        if len(s) > 4:
            if s == "unties":
                print('R4')
                hypothesised_stem = "untie"
                return_stem = return_stem_or_not(hypothesised_stem, third_sg_in_dict)
                return return_stem
            else:
                print('R3')
                hypothesised_stem = s[:-3] + "y"
                return_stem = return_stem_or_not(hypothesised_stem, third_sg_in_dict)
                return return_stem
        else:

            if s[0] not in ['a', 'e', 'i', 'o', 'u']:
                print('R4')
                hypothesised_stem = s[:-1]
                return_stem = return_stem_or_not(hypothesised_stem, third_sg_in_dict)
                return return_stem
            else:
                return ""

    if re.search(r"(?<=(o|x))es$", s) or re.search(r"(?<=(ch|sh|zz|ss))es$", s):
        print('R5')
        hypothesised_stem = s[:-2]
        return_stem = return_stem_or_not(hypothesised_stem, third_sg_in_dict)
        return return_stem

    if re.search(r"(?<=(se|ze))s$", s) and re.search(r"(?<!(sse|zze))s$", s):
        print('R6')
        hypothesised_stem = s[:-1]
        return_stem = return_stem_or_not(hypothesised_stem, third_sg_in_dict)
        return return_stem

    if s in ["has", "is", "does"]:
        print('R7')
        if s == "has":
            hypothesised_stem = "have"
        elif s == "is":
            hypothesised_stem = "have"
        else:
            hypothesised_stem = "do"
        return hypothesised_stem

    if re.search(r"(?<!(i|o|s|x|z))es$", s) and re.search(r"(?<!(ch|sh))es$", s):
        print('R8')
        hypothesised_stem = s[:-1]
        return_stem = return_stem_or_not(hypothesised_stem, third_sg_in_dict)
        return return_stem

    return ""

def add_proper_name (w,lx):
    """adds a name to a lexicon, checking if first letter is uppercase"""
    if ('A' <= w[0] and w[0] <= 'Z'):
        lx.add(w,'P')
        return ''
    else:
        return (w + " isn't a proper name")

def process_statement (lx,wlist,fb):
    """analyses a statement and updates lexicon and fact base accordingly;
       returns '' if successful, or error message if not."""
    # Grammar for the statement language is:
    #   S  -> P is AR Ns | P is A | P Is | P Ts P
    #   AR -> a | an
    #   Ns -> noun singular
    #   A -> adjective
    #   Is -> verb singular
    # We parse this in an ad hoc way.
    msg = add_proper_name (wlist[0],lx)
    if (msg == ''):
        if (wlist[1] == 'is'):
            if (wlist[2] in ['a','an']):
                lx.add (wlist[3],'N')
                fb.addUnary ('N_'+wlist[3],wlist[0])
            else:
                lx.add (wlist[2],'A')
                fb.addUnary ('A_'+wlist[2],wlist[0])
        else:
            stem = verb_stem(wlist[1])
            if (len(wlist) == 2):
                lx.add (stem,'I')
                fb.addUnary ('I_'+stem,wlist[0])
            else:
                msg = add_proper_name (wlist[2],lx)
                if (msg == ''):
                    lx.add (stem,'T')
                    fb.addBinary ('T_'+stem,wlist[0],wlist[2])
    return msg
                        
# End of PART A.

