# File: pos_tagging.py
# Template file for Informatics 2A Assignment 2:
# 'A Natural Language Query System in Python/NLTK'

# John Longley, November 2012
# Revised November 2013 and November 2014 with help from Nikolay Bogoychev
# Revised November 2015 by Toms Bergmanis


# PART B: POS tagging

from statements import *

# The tagset we shall use is:
# P  A  Ns  Np  Is  Ip  Ts  Tp  BEs  BEp  DOs  DOp  AR  AND  WHO  WHICH  ?

# Tags for words playing a special role in the grammar:

function_words_tags = [('a','AR'), ('an','AR'), ('and','AND'),
     ('is','BEs'), ('are','BEp'), ('does','DOs'), ('do','DOp'), 
     ('who','WHO'), ('which','WHICH'), ('Who','WHO'), ('Which','WHICH'), ('?','?')]
     # upper or lowercase tolerated at start of question.

function_words = [p[0] for p in function_words_tags]

def unchanging_plurals():
    unchanging_plurals = []
    nouns_sing_plur_dict = {'NN': [], 'NNS': []}
    with open("sentences.txt", "r") as f:
        for line in f:
            # add code here
            words_tags = line.split()
            for word_tag in words_tags:
                word_tag_split = word_tag.split('|')
                if word_tag_split[1] == 'NN':
                    if word_tag_split[0] not in nouns_sing_plur_dict['NN']:
                        nouns_sing_plur_dict['NN'].append(word_tag_split[0])
                else:
                    if word_tag_split[1] == 'NNS':
                        if word_tag_split[0] not in nouns_sing_plur_dict['NNS']:
                            nouns_sing_plur_dict['NNS'].append(word_tag_split[0])

    for singular_noun in nouns_sing_plur_dict['NN']:
        if singular_noun in nouns_sing_plur_dict['NNS']:
            unchanging_plurals.append(singular_noun)

    return unchanging_plurals

unchanging_plurals_list = unchanging_plurals()

def return_stem_noun_or_not(stem, plural_in_dict):
    if plural_in_dict:
        return stem
    else:
        if (stem, "NN") in brown.tagged_words():
            return stem
        else:
            return ""

def noun_stem (s):
    """extracts the stem from a plural noun, or returns empty string"""    
    # add code here

    if s in unchanging_plurals_list:
        print('R1')
        return s

    is_word = False
    if s.isalpha():
        is_word = True

    if not is_word:
        return ""

    plural_in_dict = False
    if (s, "NNS") in brown.tagged_words():
        plural_in_dict = True

    if s.endswith("men"):
        print('R2')
        hypothesised_stem = s[:-3] + "man"
        return_stem = return_stem_noun_or_not(hypothesised_stem, plural_in_dict)
        return return_stem

    if re.search(r"(?<!(s|x|y|z|a|e|i|o|u))s$", s) and re.search(r"(?<!(ch|sh))s$", s):
        print('R3')
        hypothesised_stem = s[:-1]
        return_stem = return_stem_noun_or_not(hypothesised_stem, plural_in_dict)
        return return_stem

    if re.search(r"(?<=[a|e|i|o|u])ys$", s):
        print('R4')
        hypothesised_stem = s[:-1]
        return_stem = return_stem_noun_or_not(hypothesised_stem, plural_in_dict)
        return return_stem

    if s.endswith("ies"):
        if len(s) > 4:
            if s == "unties":
                print('R6')
                hypothesised_stem = "untie"
                return_stem = return_stem_noun_or_not(hypothesised_stem, plural_in_dict)
                return return_stem
            else:
                print('R5')
                hypothesised_stem = s[:-3] + "y"
                return_stem = return_stem_noun_or_not(hypothesised_stem, plural_in_dict)
                return return_stem
        else:

            if s[0] not in ['a', 'e', 'i', 'o', 'u']:
                print('R6')
                hypothesised_stem = s[:-1]
                return_stem = return_stem_noun_or_not(hypothesised_stem, plural_in_dict)
                return return_stem
            else:
                return ""

    if re.search(r"(?<=(o|x))es$", s) or re.search(r"(?<=(ch|sh|zz|ss))es$", s):
        print('R7')
        hypothesised_stem = s[:-2]
        return_stem = return_stem_noun_or_not(hypothesised_stem, plural_in_dict)
        return return_stem

    if re.search(r"(?<=(se|ze))s$", s) and re.search(r"(?<!(sse|zze))s$", s):
        print('R8')
        hypothesised_stem = s[:-1]
        return_stem = return_stem_noun_or_not(hypothesised_stem, plural_in_dict)
        return return_stem

    if re.search(r"(?<!(i|o|s|x|z))es$", s) and re.search(r"(?<!(ch|sh))es$", s):
        print('R9')
        hypothesised_stem = s[:-1]
        return_stem = return_stem_noun_or_not(hypothesised_stem, plural_in_dict)
        return return_stem

    return ""

def tag_word (lx,wd):
    """returns a list of all possible tags for wd relative to lx"""
    # add code here

    tags = []
    if wd in function_words:
        i_word = function_words.index(wd)
        tags.append(function_words_tags[i_word][1])

    for pos in ['P', 'N', 'A', 'I', 'T']:
        words_pos = lx.getAll(pos)
        # print(words_pos)
        if wd in words_pos:
            if pos == 'P':
                tags.append('P')
            elif pos in ['N', 'I', 'T']:
                if pos == 'N':
                    n_stem = noun_stem(wd)
                    if n_stem == wd:
                        tags += ['Ns', 'Np']
                    else:
                        if n_stem == "":
                            # not sure what to do here
                            tags.append('Ns')   # or maybe don't do anything
                        else:
                            tags.append('Np')
                else:
                    v_stem = verb_stem(wd)
                    if v_stem == wd:
                        tags += [pos + 's', pos + 'p']
                    else:
                        if v_stem == "":
                            # not sure what to do here
                            tags.append(pos + 'p')   # or maybe don't do anything
                        else:
                            tags.append(pos + 's')
            else:
                tags.append('A')
    print(tags)
    return tags

def tag_words (lx, wds):
    """returns a list of all possible taggings for a list of words"""
    if (wds == []):
        return [[]]
    else:
        tag_first = tag_word (lx, wds[0])
        tag_rest = tag_words (lx, wds[1:])
        return [[fst] + rst for fst in tag_first for rst in tag_rest]

# End of PART B.